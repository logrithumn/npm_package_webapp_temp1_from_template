import PropTypes from 'prop-types';

const CSComponent = function(props = {config: {}}) {
  const {
    config,
  } = props;

  console.log(config);

  return (
    <>
      Im partner component
    </>
  );
};

CSComponent.propTypes = {
  config: PropTypes.object,
};

export default CSComponent;