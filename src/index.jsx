'use strict';
import Layout from '@koc/npm_package_webapp_helper/src/Layout';
import { StrictMode } from 'react';
import { createRoot } from 'react-dom/client';
import CSComponent from './component';
import config from './config';
const container = document.getElementById('app');
const root = createRoot(container);

function init() {
  root.render(
    <StrictMode>
      <Layout>
        <CSComponent config={config} />
      </Layout>
    </StrictMode>
  );
}

init();